/**
 * Creates a new Answer object
 * @param {int} index
 * @param {object} data
 * @param {Question} question
 * @constructor
 */
function Answer(index, data, question) {
    this.id = index;
    this.text = markdownToHtml(data['text']);
    this.isCorrect = data['isCorrect'];
    this.isSelected = false;
    this.question = question;
}

/**
 * Creates the HTML for this answer panel and returns it
 * @return {string}
 */
Answer.prototype.createHTML = function () {
    return '<div class="mui-panel answer" onclick="quiz.currentQuestion.clickAnswer(' + this.id + ')" id="answer' + this.id + '" data-isCorrect="' + this.isCorrect + '">' +
        '<i class="fa fa-square-o" aria-hidden="true" id="icon' + this.id + '"></i> &nbsp;' +
        this.text +
        '</div>';
};

/**
 * Checks the solution of this answer. ("Is it checked and should it be checked?")
 * @return {boolean}
 */
Answer.prototype.checkSolution = function () {
    quiz.allowClickOnAnswer = false;
    if ((this.isCorrect && this.isSelected) || (!this.isCorrect && !this.isSelected)) {
        // user was right
        $("#answer" + this.id).addClass("correct");
        return true;
    } else {
        // user was wrong
        $("#answer" + this.id).addClass("incorrect");
        return false;
    }
};

/**
 * Clicks this answer. Toggles the checked status and sets / removes the check symbol
 */
Answer.prototype.click = function () {
    if (!quiz.allowClickOnAnswer) return;
    if (this.isSelected) {
        $('#icon' + this.id).attr('class', 'fa fa-square-o');
        this.isSelected = false;
    } else {
        $('#icon' + this.id).attr('class', 'fa fa-check-square-o');
        this.isSelected = true;
    }
};
