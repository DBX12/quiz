/**
 * Creates a new Chapter object
 * @param {int} index
 * @param {object} data
 * @param {Quiz} quiz
 * @constructor
 * @property {int} id
 * @property {string} name
 * @property {Question[]} questions
 * @property {Category} category
 * @property {Quiz} quiz
 */
function Chapter(index, data, quiz) {
    this.id = index;
    this.name = data['name'];
    this.questions = [];
    this.quiz = quiz;
    this.category = new Category(this.name,this.quiz);

    data['questions'].forEach(function (question_data, index) {
        var q = new Question(index, question_data, this);
        this.questions.push(q);
        this.category.children.push(new SubItem(q.short, this.id, q.id, this.quiz));
    }, this);

    this.questions[0].hasPrev = false;
    this.questions[this.questions.length - 1].hasNext = false;
}

Chapter.prototype.isCleared = function () {
    return this.quiz.storage.getChapterProgress(this.id) === this.questions.length;
};
