/**
 * Creates a SubItem object for the sidedrawer. SubItems represent Questions in the sidedrawer.
 * @param _display
 * @param chapter_id
 * @param question_id
 * @param {Quiz} quiz
 * @constructor
 * @property {string} display The displayed name of the subitem
 * @property {string} link Where to go onclick
 */
function SubItem(_display, chapter_id, question_id, quiz) {
    this.display = _display;
    this.link = "?chapter=" + chapter_id + "&question=" + question_id;
    this.quiz = quiz;
    this.colorIndex = Math.round(this.quiz.storage.getQuestionProgress(chapter_id, question_id) * 10);
    this.colors = [
        "#f08080", // 0.00 - 0.09
        "#E29082", // 0.10 - 0.19
        "#DD9683", // 0.20 - 0.29
        "#D89C84", // 0.30 - 0.39
        "#D3A185", // 0.40 - 0.49
        "#CAAC86", // 0.50 - 0.59
        "#C0B788", // 0.60 - 0.69
        "#B6C28A", // 0.70 - 0.79
        "#ADCD8B", // 0.80 - 0.89
        "#A3D88D", // 0.90 - 0.99
        "#90ee90" // 1.00
    ]
}

/**
 * Create the HTML for the SubItem and return it
 * @returns {string}
 */
SubItem.prototype.getHTML = function () {
    var out = "";
    if (this.quiz.disabledFeatures.indexOf('css_01') === -1) {
        out = "<li class=\"full_width_colors\" style=\"background-color: " + this.colors[this.colorIndex] + "\">";
    } else {
        out = "<li>";
        if (this.colorIndex === 10)
            out += "<i class='fa fa-check' aria-hidden='true'></i>&nbsp;";
        else
            out += "<i class='fa fa-times' aria-hidden='true'></i>&nbsp;"
    }

    return out + "<a href=\"" + this.link + "\">" + this.display + "</a></li>";
};