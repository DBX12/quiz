/**
 * Initializes a storage object
 * @param {String} quizname
 * @constructor
 */
function Storage(quizname) {
    this.quizname = quizname;
}

/**
 *
 * @param {string|int} chapterId
 * @param {string|int} questionId
 * @param {Number} score
 */
Storage.prototype.setQuestionProgress = function (chapterId, questionId, score) {

    this.updateChapterProgress(chapterId, questionId, score);
    localStorage.setItem(this.buildKey(chapterId, questionId), score.toString());
};

/**
 * Updates the chapter progress
 * @param chapterId
 * @param questionId
 * @param score
 */
Storage.prototype.updateChapterProgress = function (chapterId, questionId, score) {
    var oldScore = this.getQuestionProgress(chapterId, questionId);
    var newScore = this.getChapterProgress(chapterId) - oldScore + score;
    localStorage.setItem(this.quizname + "~" + chapterId, newScore);
};

/**
 * Gets the progress of a chapter
 * @param chapterId
 * @return {number}
 */
Storage.prototype.getChapterProgress = function (chapterId) {
    return Number(localStorage.getItem(this.quizname + "~" + chapterId)) || 0.0;
};

/**
 * Gets the progress of a question
 * @param {string|int} chapter
 * @param {string|int} question
 * @return {number}
 */
Storage.prototype.getQuestionProgress = function (chapter, question) {
    return Number(localStorage.getItem(this.buildKey(chapter, question))) || 0.0;
};

/**
 * Creates the unique storage key for a question
 * @param {string|int} chapterId
 * @param {string|int} questionId
 * @return {string}
 */
Storage.prototype.buildKey = function (chapterId, questionId) {
    return this.quizname + "/" + chapterId + "/" + questionId;
};