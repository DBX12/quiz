/**
 * Creates a new Quiz object
 * @param {object} data
 * @constructor
 * @property {string} name
 * @property {Chapter[]} chapters
 * @property {Question} currentQuestion
 * @property {string[]} disabledFeatures
 * @property {Storage} storage
 */
function Quiz(data) {
    this.name = data['name'];
    this.chapters = [];
    this.currentQuestion = {};
    this.allowClickOnAnswer = true;
    this.disabledFeatures = data['disable'] || [];
    this.storage = new Storage(this.name);
    data['chapters'].forEach(function (chapter_data, index) {
        this.chapters.push(new Chapter(index, chapter_data, this))
    }, this);
    $('span.quiz-name').text(this.name);
}

/**
 * Displays the question selected through chapter and question
 * @param {int} chapter
 * @param {int} question
 */
Quiz.prototype.displayQuestion = function (chapter, question) {
    this.currentQuestion = this.chapters[chapter].questions[question];
    this.currentQuestion.display();
};