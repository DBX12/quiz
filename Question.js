/**
 * Creates a new Question object
 * @param index {int}
 * @param data {object}
 * @param  {Chapter} chapter
 * @constructor
 * @property {int} id
 * @property {string} short
 * @property {string} text
 * @property {Answer[]} answers
 * @property {boolean} hasNext
 * @property {boolean} hasPrev
 */
function Question(index, data, chapter) {
    this.id = index;
    this.short = "Question " + this.id;
    this.text = markdownToHtml(data['text']);
    this.hint = null;
    this.answers = [];
    this.hasNext = true;
    this.hasPrev = true;
    this.chapter = chapter;

    // set custom short name
    if ("short" in data && data["short"].length > 0)
        this.short = markdownToHtml(data["short"]);

    // set hint if available
    if ("hint" in data  && data["hint"].length > 0)
        this.hint = markdownToHtml(data["hint"]);

    data['answers'].forEach(function (data, index) {
        this.answers.push(new Answer(index, data, this));
    }, this);
}

/**
 * Displays this question
 */
Question.prototype.display = function () {
    $('#h1-question').html(this.short);
    $('#question-text').html(this.text);
    var html = '';
    this.answers.forEach(function (answer, id) {
        /** @param {Answer} answer */
        html += answer.createHTML();
    });
    $('#answer-container').prepend(html);
    if (!this.hasNext)
        $('#nextBtn').prop('disabled', true);
    if (!this.hasPrev)
        $('#prevBtn').prop('disabled', true);
    if (this.hint !== null) {
        $('#hint_container').css('visibility', 'visible');
    }
};

/**
 * Shows the next question (if available)
 */
Question.prototype.next = function () {
    if (this.hasNext)
        window.location.href = "?chapter=" + this.chapter.id + "&question=" + (Number(this.id) + 1);
};

/**
 * Shows the previous question (if available)
 */
Question.prototype.previous = function () {
    if (this.hasPrev)
        window.location.href = "?chapter=" + this.chapter.id + "&question=" + (Number(this.id) - 1);
};

/**
 * Validates the solution of the user.
 * @returns {int} score Between 0 and 1. 0 -> everything wrong, 1 -> everything correct
 */
Question.prototype.validate = function () {
    $('#showSolutionBtn').attr('disabled', 'disabled');
    var good = 0;
    var bad = 0;
    this.answers.forEach(function (answer) {
        if (answer.checkSolution())
            good++;
        else
            bad++;
    });
    var score = good / (good + bad);
    this.chapter.quiz.storage.setQuestionProgress(this.chapter.id, this.id, score);
    $('#score_progressbar').css('width', score * 100 + "%");
    return score;
};

/**
 * Handles the click on an answer
 * @param {int} id
 */
Question.prototype.clickAnswer = function (id) {
    this.answers[id].click();
};

Question.prototype.revealHint = function () {
    $('#hint_text').html(this.hint);
};

Question.prototype.isCleared = function () {
    return this.chapter.quiz.storage.getQuestionProgress(this.chapter.id, this.id) === 1.0;
};
