// expand markdown language here, you have to escape all regex control characters like * or -
var map = {
    "\\*\\*": {
        "begin": "<span style=\'font-weight: bold\'>",
        "end": "</span>",
        "on": false
    },
    "__": {
        "begin": "<span style=\"text-decoration: underline\">",
        "end": "</span>",
        "on": false
    },
    "~~": {
        "begin": "<span style=\"text-decoration: line-through; \">",
        "end": "</span>",
        "on": false
    },
    "``": {
        "begin": "<span style=\"font-family: monospace; padding: 1px 3px; border-radius: 3px; background-color: lightgray;\">",
        "end": "</span>",
        "on": false
    }
};

function markdownToHtml(markdown) {
    // if it were that easy...
    var html = markdown;
    Object.keys(map).forEach(function (key) {
        map[key].on = false;
        var regex = new RegExp(key, "g");
        html = html.replace(regex,
            function (match, offset, string) {
                if (map[key].on === true) {
                    map[key].on = false;
                    return map[key].end;
                } else {
                    map[key].on = true;
                    return map[key].begin;
                }
            }
        );
    });
    return html;
}
