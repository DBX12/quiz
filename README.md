#Simple quiz system

With this JavaScript project, you can create a quiz with a simple JSON file. A webserver or internet connection is not 
required, everything runs in your Browser. Just open `file://<path-to-Quiz-dir>/quiz.html` in a browser of your choice.\*

Edit or replace the sample `quiz.json` to create your quiz.

\*[Lynx](https://en.wikipedia.org/wiki/Lynx_(web_browser)) is not supported, sorry.

## Markdown Support
Markdown is supported in the Question objects for short, hint, text and answer texts. (Basically any string). It is a 
reduced markup (currently only supporting bold \*\*, underline \_\_, strike-through ~~ and code ``)

## Workaround for Chrome

Chrome does not trust localhost in its same origin policy. A workaround would be disabling web security for the session.
Add the flag `--disable-web-security` to your starter.

## Progress display

Your progress is now displayed in the left menu. If it breaks the sidebar for you, please file an issue with your
browser, its version and your OS (sadly, Broswers behave differently on different OS).
You can partly disable the feature by adding "css_01" to the `disable` array in the `quiz.json`. This will remove the
colors (and a CSS "hack") and displays a checkmark or a cross instead.

## Hacks

The quiz uses some hacks (pieces of codes of which I suspect they are bad practice but I don't know it better and it works)
If they break anything for you, add the according name to the `disable` array in the quiz.json. In fact, even this is hacky
as you have to alter if every time you load a new quiz. So this will be the only hack which cannot be disabled through
the `disable` configuration.

Lookup the error description and disable the according hack. If it does not fix your issue, please file one on Bitbucket.

| Problem description                                                                                 | Hack name |
|:----------------------------------------------------------------------------------------------------|:---------:|
| I hate the colors of the sidebar. I would rather have icons.  My sidebar is buggy since the update. | css_01    |
| Disabling the hack `css_01` did not fix it for me.                                                  | css_02    |
