/**
 * Creates a Sidedrawer object (this will populate the sidedrawer - big surprise, eh?)
 * @param {Quiz} quiz
 * @constructor
 * @property {Category[]} categories
 */
function Sidedrawer(quiz) {
    this.categories = [];

    quiz.chapters.forEach(function (chapter) {
        this.categories.push(chapter.category);
    }, this);
}

/**
 * Populates the sidedrawer with Categories and SubItems
 */
Sidedrawer.prototype.populate = function () {
    var html = '';
    this.categories.forEach(function (category) {
        html += category.getHTML();
    });
    $('#sidedrawer-menu-top').prepend(html);
};