/**
 * Creates a category for the sidedrawer
 * @param _name
 * @param {Quiz} quiz
 * @constructor
 * @property {string} name
 * @property {SubItem[]} children
 * @property {Quiz} quiz
 */
function Category(_name, quiz) {
    this.name = _name;
    this.children = [];
    this.quiz = quiz;
}

/**
 * Create the HTML for the Category and return it
 * @returns {string}
 */
Category.prototype.getHTML = function () {
    var html = "";
    if (this.quiz.disabledFeatures.indexOf('css_02') === -1)
        html = "<li><strong data-hack_css02='true'>" + this.name + "</strong><ul>";
    else
        html = "<li><strong>" + this.name + "</strong><ul>";

    this.children.forEach(function (subItem) {
        /** @param {SubItem} subItem */
        html += subItem.getHTML();
    });
    html += '</ul></li>';
    return html;
};